﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class PlayerManage : MonoBehaviour
{
    PhotonView PV;

    GameObject controller;

     void Awake()
    {
        PV = GetComponent<PhotonView>();
    }
    void Start()
    {
        CreatController();

    }
  void  CreatController()
    {
        Transform spawnpoint = spawnManager.Instance.GetSpawnPiont();

        if (PV.IsMine && PhotonNetwork.IsMasterClient)
        {
            controller = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerRed"), spawnpoint.position, spawnpoint.rotation, 0 , new object[] { PV.ViewID});
        }
        if (PV.IsMine && !PhotonNetwork.IsMasterClient)
        {
            controller = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerBlue"), spawnpoint.position, spawnpoint.rotation, 0, new object[] { PV.ViewID });
        }

        }

    public void Die()
    {
        PhotonNetwork.Destroy(controller);
        CreatController();
    }
}
