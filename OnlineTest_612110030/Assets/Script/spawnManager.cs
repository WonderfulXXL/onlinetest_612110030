﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnManager : MonoBehaviour
{
    public static spawnManager Instance;

    spawnPoint[] spawnpoints;

    void Awake()
    {
        Instance = this;
        spawnpoints = GetComponentsInChildren<spawnPoint>();

    }

    public Transform GetSpawnPiont()
    {
        return spawnpoints[Random.Range(0, spawnpoints.Length)].transform;
    }
}
