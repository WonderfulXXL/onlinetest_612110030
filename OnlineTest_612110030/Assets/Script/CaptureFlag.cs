﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using System;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class CaptureFlag : MonoBehaviourPunCallbacks
// Start is called before the first frame update
{
    public float CapFlaq = 0f;
    public float CapFlaqRed = 0f;
    public bool CapflagRed = false;
    public bool CapflagBlue = false;
    public Text textRed;
    public Text textBlue;

    bool isTimerRunning;
    private float startTime;
    private float startTime2;
    public float currentCountDown;
    public float currentCountDown2;

    public float score = 1;
    public float scoreX = 1;
    public float cap;
    public float cap2;

    public delegate void CountdownTimerHasExpired();

    public static event CountdownTimerHasExpired OnCountdownTimerHasExpired;

    private void Start()
    {
        Launcher.OnFirstSetting += StartTime;
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" )
        {
            CapflagRed = true;
            CapflagBlue = true;
            score = 5;
        }
        if (other.gameObject.tag == "Player2")
        {
            CapflagBlue = true;
            CapflagRed = true;
            scoreX = 5;
        }
        
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            CapflagRed = false;
          
        }
        if (other.gameObject.tag == "Player2")
        {
            CapflagBlue = false;
        }

    }
    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {     
        GetStartTime(propertiesThatChanged);

        GetStartTime2(propertiesThatChanged);
    }
    public void GetStartTime(Hashtable propertiesThatChanged)
    {
        object startTimFromProp;
        if (propertiesThatChanged.TryGetValue(GameSetting.Start_gameTime, out startTimFromProp))
        {
            isTimerRunning = true;
            startTime = (float)startTimFromProp;
        }
       
    }
    public void GetStartTime2(Hashtable propertiesThatChanged)
    {
        object startTimFromProp;
        if (propertiesThatChanged.TryGetValue(GameSetting.Start_gameTime2, out startTimFromProp))
        {
            isTimerRunning = true;
            startTime2 = (float)startTimFromProp;
        }

    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
            GetStartTime(PhotonNetwork.CurrentRoom.CustomProperties);
            GetStartTime2(PhotonNetwork.CurrentRoom.CustomProperties);

    }
    public void StartTime()
    {
        Hashtable props = new Hashtable
        {
            {GameSetting.Start_gameTime,(float)PhotonNetwork.Time }
        };
        Hashtable props2 = new Hashtable
        {
            {GameSetting.Start_gameTime2,(float)PhotonNetwork.Time }
        };

        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
        PhotonNetwork.CurrentRoom.SetCustomProperties(props2);
    }
    string ConvertTime(float seconds)
    {
        double/* hh = Math.Floor(seconds / 3600),
              mm = Math.Floor(seconds / 60) % 60,*/
              ss = Math.Floor(seconds) % 100;
        return /*hh.ToString("00") + " :" +
            mm.ToString("00") + ":" +*/
            ss.ToString("000");
    }
    public void Update()
    {

        if (CapflagRed == true)
        {
            float timer = (float)PhotonNetwork.Time - startTime;
            currentCountDown = CapFlaqRed + timer;
             cap = currentCountDown + score ;
        }
        if (CapflagRed == false)
        {
            score = 1;
        }
        if (CapflagBlue == true)
        {
  
            float timer2 = (float)PhotonNetwork.Time - startTime2;
            currentCountDown2 = CapFlaq + timer2;
            cap2 = currentCountDown2 + scoreX ;
        }
        if (CapflagBlue == false)
        {
            scoreX = 1;
        }

        
        textRed.text = " CapflagRed : " + ConvertTime(cap);
        textBlue.text = " CapflagBlue : " + ConvertTime(cap2);


        if (cap >= 100f)
        {
            Debug.Log("RedWin");
            isTimerRunning = false;

            textRed.text = string.Empty;
            textBlue.text = string.Empty;

            if (OnCountdownTimerHasExpired != null)
            {
                OnCountdownTimerHasExpired();
            }

        }
        if (currentCountDown2 >= 100f)
        {
            Debug.Log("BlueWin");     
            isTimerRunning = false;

            textRed.text = string.Empty;
            textBlue.text = string.Empty;

            if (OnCountdownTimerHasExpired != null)
            {
                OnCountdownTimerHasExpired();
            }
        }
            
    }
    private void OnCountdownTimerIsExpired()
    {
        if (PhotonNetwork.CurrentRoom == null)
            return;

        Hashtable props = new Hashtable
        {
            {GameSetting.GAMEOVER, true }
        };

        PhotonNetwork.CurrentRoom.SetCustomProperties(props);
    }
    public override void OnEnable()
    {
        base.OnEnable();
        OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
    }
    public override void OnDisable()
    {
        base.OnDisable();
        OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
    }
}

