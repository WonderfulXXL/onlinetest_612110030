﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;


public class Launcher : ConnectAndJoinRandom 
{
    public static Launcher Instance;

    public bool isFirstSetting = false;
    public GameObject Capture;
    public bool isGameStart = false;


    public delegate void FirstSetting();
    public static event FirstSetting OnFirstSetting;




    private void Awake()
    {
        Instance = this;
        OnFirstSetting += FirstSettingCanvas;
    }


    void Update()
    {
        if (PhotonNetwork.IsMasterClient != true)
            return;

        if (isGameStart == true)
        {
            if (isFirstSetting == false)
                OnFirstSetting();

            if(isFirstSetting == true)
            {
               
            }
        }
    }
    private void FirstSettingCanvas()
    {
        isFirstSetting = true;
             
    }
    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        base.OnRoomPropertiesUpdate(propertiesThatChanged);
    }
     public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Launcher.Instance.SpawnPlayer();

    }
  
    public void SpawnPlayer()
    {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PlayerManage"), Vector3.zero, Quaternion.identity);
             isGameStart = true;
    }

}

    
