﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerController : MonoBehaviourPunCallbacks, IDamage
{
    [SerializeField] GameObject cameraHolder;

    [SerializeField] float mouseSensitive, spritSpeed, walkSpeed, jumpForce, smoothTime;

    Rigidbody rb;
    PhotonView PV;

    
    
    const float maxHealth = 100f;
    float currentHealth = maxHealth;

    Text text;
    Vector3 moveAmount;
    float verticalLookRotation;
    Vector3 smoothMoveVelocity;
    public bool grounded;

    PlayerManage playerManager;


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();

        playerManager = PhotonView.Find((int)PV.InstantiationData[0]).GetComponent<PlayerManage>();

    }
    void Update()
    {
        if (!PV.IsMine)
            return;
        Look();
        Move();
        Jump();

        if(transform.position.y < -50f)
        {
            Die();
        }
    }

    void Start()
    {
        if (PV.IsMine)
        {
            // EquipItem(0);
        }
        else
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(rb);
        }

    }
    void Look()
    {
        transform.Rotate(Vector3.up * Input.GetAxisRaw("Mouse X") * mouseSensitive);
        verticalLookRotation += Input.GetAxisRaw("Mouse Y") * mouseSensitive;
        verticalLookRotation = Mathf.Clamp(verticalLookRotation, -90f, 90f);

        cameraHolder.transform.localEulerAngles = Vector3.left * verticalLookRotation;
    }
    void Jump()
    {

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            rb.AddForce(transform.up * jumpForce);
        }
    }
    public void SetGroundedState(bool _grounded)
    {
        grounded = _grounded;
    }
    void Move()
    {
        Vector3 moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

        moveAmount = Vector3.SmoothDamp(moveAmount, moveDir * (Input.GetKey(KeyCode.LeftShift) ? spritSpeed : walkSpeed), ref smoothMoveVelocity, smoothTime);
    }
    void FixedUpdate()
    {
        if (!PV.IsMine)
            return;
        rb.MovePosition(rb.position + transform.TransformDirection(moveAmount) * Time.fixedDeltaTime);
    }
    public void TakeDamage(float damage)
    {
        PV.RPC("RPC_TakeDamage", RpcTarget.All, damage);
    }
    [PunRPC]
   void RPC_TakeDamage(float damage)
    {
        if (!PV.IsMine)
            return;

        Debug.Log(damage);
        currentHealth -= damage;
        if (currentHealth < 0)
        {
            Die();
        }
    }
    void Die()
    {
        playerManager.Die();
    }

   
  

}
